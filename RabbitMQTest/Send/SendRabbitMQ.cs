﻿using System;
using RabbitMQ.Client;
using System.Text;

namespace Send
{
    class SendRabbitMQ
    {
        public void SendRabbit(string message)
        {
            var factory = new RabbitMQ.Client.ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "user",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                
                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                                     routingKey: "user",
                                     basicProperties: null,
                                     body: body);
                
            }

         
        }
    }
}
