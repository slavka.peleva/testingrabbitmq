﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

using System.Text;

namespace Receive
{
    public partial class UserContext : DbContext
    {

        public UserContext() : base("UserContext") { }
        public virtual DbSet<UserRabbitMQ> Users { get; set; }

    }
}
