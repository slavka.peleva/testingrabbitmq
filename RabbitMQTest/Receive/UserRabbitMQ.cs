namespace Receive
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("USERSRABBITMQ")]
    public partial class UserRabbitMQ
    {
        [Key]
        [Column(Order = 0)]
        public int UserID { get; set; }

    
        public string Username { get; set; }
    }
}
