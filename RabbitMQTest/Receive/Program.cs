﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Collections.Specialized;


namespace Receive
{
    class Program
    {
        static void Main(string[] args)
        {
          // Connect();
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "user",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Received {0}", message);
                    using (var context = new UserContext())
                    {

                        var user = new UserRabbitMQ
                        {
                            
                            Username = message
                            
                        };
                        Console.WriteLine("DB " + user +" message ****** " + message );
                        context.Users.Add(user);
                        context.SaveChanges();

                    }
                };
                channel.BasicConsume(queue: "user",
                                     autoAck: true,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }

        public static void Connect()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString =
            "Data Source=WL-SPELEVA\\SQLEXPRESS;" +
            "Initial Catalog=Test;" +
            "Integrated Security=SSPI;";
            conn.Open();
        }
    }
}

